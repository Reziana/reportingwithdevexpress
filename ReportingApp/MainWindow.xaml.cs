﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ReportingApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ReportDesigner_Loaded(object sender, RoutedEventArgs e)
        {
            XtraReport multiValueReport = new XtraReport();
            XtraReport singleValueReport = new XtraReport();
            multiValueReport.LoadLayout(System.IO.Path.Combine(Environment.CurrentDirectory, @"DesignedReports\MultiValueReport.repx"));
            singleValueReport.LoadLayout(System.IO.Path.Combine(Environment.CurrentDirectory, @"DesignedReports\SingleValueReport.repx"));

            _reportDesigner.OpenDocument(multiValueReport);
            _reportDesigner.OpenDocument(singleValueReport);

        }
    }
}
